#include <sys/types.h>         
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

//checar esto man getsockopt

//eth.type==0x0coc
//int index;
unsigned char mimac[6];//mi mac 
unsigned char mibroad[8];
unsigned char mimascara[6];
unsigned char miip[4];
unsigned char MACbroadcast[6]={0xff,0xff,0xff,0xff,0xff,0xff};//mac destino 
unsigned char MacDestino[6];
unsigned char IPDestino[]={0x0A,0xD0,0xDD,0x80};//ip destino hay que hacerlo de forma dinamica 
unsigned char tramaEnv[100];
unsigned char recivTrama[200];
unsigned char ethertype[]={0x0c,0x0c};
//algunos protocolos
unsigned char arp[]={0x08,0x06};
unsigned char ether[]={0x00,0x01};
unsigned char proip[]={0x08,0x00};
unsigned char arprequest[]={0x00,0x01};



void estructuraTrama( unsigned char *trama);
void enviarTrama(int ds,int index,unsigned char *trama);
void recibeTrama(int ds,unsigned char * trama,int len);
void imprimeTrama(unsigned char *trama,int len);
void PideIP(void);

void PideIP(void)
{
	int i;
	puts("\nEscriba la direccion IP destino\n");//aqui es la idea de pedir los datos de la dieccion ip pero hay que cambiarlos a exadecimal para que funcione jaja 
	/*for(i=0;i<4;i++)
	{
		scanf("%x",&IPDestino[i]);
	}*/
	puts("la direccion ip que ingreso es :");
	for(i=0;i<4;i++)
	{
		printf(" %d-",IPDestino[i]);
	}
	
	puts("");
}
int main()
{

     int packet_socket, indice;
     packet_socket=socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ARP));
     if(packet_socket==-1)
     perror("\n Error al abrir el socket\n");
     else
     {
          printf("\nExito al abrir el socket\n");
          indice=obtenerDatos(packet_socket);
          PideIP();
	  estructuraTrama(tramaEnv);//genera la trama de arp
          enviarTrama(packet_socket,indice,tramaEnv);//envia la trama de arp 
          recibeTrama(packet_socket,recivTrama,1514);//recive la trama de arp
          close(packet_socket);
     }
     return 0;
}


void estructuraTrama( unsigned char *trama)//estructura la trama para arp
{
int i;
//unsigned short tipo=htons(ETH_P_ARP);
//unsigned short tipo2=htons(ARPHRD_ETHER);
//unsigned short tipo3=htons(ETH_P_IP);

	printf("\nestructurando trama de arp\n");
	//puts("agregando mac broadcast");
		memcpy(trama+0,MACbroadcast,6);//trama de broadcast
	//puts("agregando mi mac");
		memcpy(trama+6,mimac,6);//mi mac 
	//puts("agregando protocolo arp");
		memcpy(trama+12,arp,2);//protocolo de arp
	//puts("agregando protocolo ethernet");
		memcpy(trama+14,ether,2);//protocolo de ethernet
	//puts("agregando el protocolo de ip");
		memcpy(trama+16,proip,2);//tipo de protocolo ip
	//puts("agregando el tamaño de direccion mac que usamos ");
		trama[18]=6;//tamaño de la direccion mac
	//puts("agregando el tamaño de la direccion ip que usamos ");
		trama[19]=4;//tamaño de la direccion ip
	//puts("agregando el mensaje a usar o indicando que es una pregunta de arp");
		memcpy(trama+20,arprequest,2);//arp request
	//puts("agregando mi mac dentro del mensaje de arp");
		memcpy(trama+22,mimac,6);//mi mac
	//puts("agregado mi ip dentro del mensaje de ip ");
		memcpy(trama+28,miip,4);//mi ip
	//puts("agregando mac destino dentro del mensaje ip");//en este caso es cero ya que eso lo agrega la trama que responde
//		memcpy(trama+32,0x00,6);//mac destino
	//puts("agregando ip destino ");//esta se agrega ya que estamos preguntnado por es ip en el mensaje 
		memcpy(trama+38,IPDestino,4);//ip destino  
	
	puts("imprimiendo tramaa enviar ");
	for(i = 0; i<42; i++)
	{
		printf("%.2x-", trama[i]);
	}
	puts("");
	
}
void enviarTrama(int ds,int index,unsigned char *trama)//genera la trama para arp
{
	int tam,arpval,opval=1,i;
	struct sockaddr_ll nic;
	memset(&nic,0x00,sizeof(nic));
	nic.sll_family=AF_PACKET;
	nic.sll_protocol=htons(ETH_P_ARP);
	nic.sll_ifindex=index;
	
	//arpval=setsockopt(ds,SOL_SOCKET,SO_BROADCAST,&opval,sizeof(opval));//activar la opcion del socket para poder mandar el mensaje de arp
	if(arpval==-1)
	{
		perror("\nNo se pudo activar el sockete para ARP\n");
	}
	else
	{	
		tam=sendto(ds,trama,42,0,(struct sockaddr *)&nic,sizeof(nic));//generar la trama por el socket
		if(tam==-1)
		{
			perror("\nerror al enviar\n");
		}
		else
		{
			printf("\n");
			for(i=0;i<42;i++)
			{
				printf("%.2x ",trama[i]);
			}
			printf("\nexito fue enviada la trama de arp\n");
		}
	}
}

int obtenerDatos(int ds)
{
	int i;
	int index;
	char nombre[10];
	/*unsigned char mac[6];
	unsigned char broad[8];
	unsigned char mas[6];
	unsigned char ip[8];*/
	unsigned char pe[20];
	
	struct ifreq interfaz;
	printf("Insertar nombre \n");
	scanf("%s",nombre);
	strcpy(interfaz.ifr_name,nombre);
	if(ioctl(ds,SIOCGIFINDEX,&interfaz)==-1)
		perror("\n error al obtener el indice");
	else
	{
		index=interfaz.ifr_ifindex;
		printf("\nEl indice es: 	%d\n",index);
		
		//Direccion MAC
		if(ioctl(ds,SIOCGIFHWADDR,&interfaz)==-1)//obtiene la direccion mac 
		{
			perror("\n Error");
		}
		else
		{
			memcpy(mimac, interfaz.ifr_dstaddr.sa_data,6);
			printf("La direccion mac es:\n");
			for(i = 0; i<6; i++)
			{
				printf("%.2x-", mimac[i]);
			}
		}
		//DireccionBroadcast
		if(ioctl(ds,SIOCGIFBRDADDR,&interfaz)==-1)//direccion de mi broadcast
		{
			perror("\nError");
		}
		else
		{	
		    memcpy(mibroad, interfaz.ifr_hwaddr.sa_data,8);		
		    printf("\nLa direccion de broadcast es: \n");
			for(i = 2; i<6; i++)
			{
				printf("%d.", mibroad[i]);
			}
		}
		//Mascara de subred
		if(ioctl(ds,SIOCGIFNETMASK,&interfaz)==-1)//mascara de subred
		{
			perror("\nError");
		}
		else
		{		
			memcpy(mimascara, interfaz.ifr_netmask.sa_data,6);
			printf("\nLa direccion de mascara de subred es: \n");
			
			for(i = 2; i<6; i++)
			{
				printf("%d.", mimascara[i]);
			}
			
		}
		//Direccion IP
		if(ioctl(ds,SIOCGIFADDR,&interfaz)==-1)//direccion ip
		{
			perror("\nError");
		}
		else
		{	
			memcpy(miip, interfaz.ifr_addr.sa_data+2,4);		
			printf("\nLa direccion IP es: \n");
			
			for(i = 0; i<4; i++)
			{				
				printf("%d-", miip[i]);
			}
		}
		
		printf("\n");
		
	}
	return index;	
	
}

void imprimeTrama(unsigned char *trama,int len)
{
	int i;
	for (i=0;i<len;i++)
	{
		if(i%16==0)
			printf("\n");
		printf("%.2x ",trama[i]);
	}	
}
void recibeTrama(int ds,unsigned char * trama,int len)
{
	int tam,i,ValoMensajeARPNTHOS;
	while(1)
	{
		tam=recvfrom(ds,trama,len,0,NULL,0);
		if(tam==-1)
			perror("\n error al recibir la trama de ARP");
		else
		{
			//ValoMensajeARPNTHOS=ntohs(ARPOP_REQUEST);
			//if((ntohs(ValoMensajeARPNTHOS)==ARPOP_REPLY) && !memcmp(trama,IPDestino,4))//checar bien en donde biene la direccion ip de la computaodra que manda el mensaje para sacar su mac se supone que ahi esta la ip del quien responde en este caso es la ip de la quien pregunto
			if((memcmp(trama+28,IPDestino,4)==0)) // la trama que recive es la direccion mac / se checa que corresponda la ip del quien la envia con la ip del quien pregunte 
			{
				printf("\nLa direccion ip ");
				for(i=0;i<4;i++)
				{
					printf("%d-",IPDestino[i]);
				}
				printf(" tiene una mac :");
				for(i = 6; i<12; i++)
				{
				printf("%.2x-", trama[i]);
				}
			}
			
		}
	puts("");
	break;
	}	
}











//wireshark&
//estudiar como funciona el protocolo arp

//Puerta de enlace
		/*if(ioctl(ds,SIOCGIFHWADDR,&interfaz)==-1)
		{
			perror("\nError");
		}
		else
		{	
		    memcpy(pe, interfaz.ifr_hwaddr.sa_data,20);		
		    printf("\nLa direccion de broadcast es: \n");
			for(i = 2; i<20; i++)
			{
				printf("%d.", pe[i]);
			}
		}*/
